/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.floorwatch.web.util;

import com.floorwatch.common.utils.UserUtilities;
import com.floorwatch.entities.Users;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author daledavis
 */
public class UserUtilitiesTest {
    
    public UserUtilitiesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of authenticate method, of class UserUtilities.
     */
    @org.junit.Test
    public void testCreateAndAuthenticate() {
        String salt = UserUtilities.generateSalt();
        System.out.println("Salt: " + salt);
        String password = "4court";
        String encryptedPassword = UserUtilities.encryptPassword(password, salt);
        System.out.println("Encrypted Password: " + encryptedPassword);
        Users user = new Users();
        user.setPassword(encryptedPassword);
        user.setSalt(salt);
        boolean expResult = true;
        boolean result = UserUtilities.authenticate(user, password);
        assertEquals(expResult, result);
    }



}
