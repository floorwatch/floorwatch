package com.floorwatch.web;

import com.floorwatch.common.utils.UserUtilities;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.UsersFacade;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * <H3>ChangePasswordBean</H3>
 *
 * ManagedBean for the FloorWatch change password panel
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="changePassword")
@ViewScoped
public class ChangePasswordBean {
    
    private Users user;
    private String username;
    private String password;
    private String passwordStyle;
    private String newPassword;
    private String newPasswordStyle;
    private String newPasswordConfirm;
    private String newPasswordConfirmStyle;
    private static Logger log = Logger.getLogger(ChangePasswordBean.class);
    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create ChangePasswordBean
     *
     * @author  Dale Davis
     * @date    1/14/2016
     */
    public ChangePasswordBean() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        navigationBean.setMainSelectedIndex(0);
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    private void validateForm() {
        
        // Reset form fields
        passwordStyle = "valid-input";
        newPasswordStyle = "valid-input";
        newPasswordConfirmStyle = "valid-input";
        
        // Required fields
        boolean requiredFieldsMissing = false;       

        if (password == null || password.length() == 0){
            passwordStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        
        if (newPassword == null || newPassword.length() == 0){
            newPasswordStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        
        if(newPasswordConfirm == null || newPasswordConfirm.length() == 0) {
            newPasswordConfirmStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        
        if(requiredFieldsMissing){
            throw new RuntimeException("There are required fields missing.");
        }
        
        // Check that the password field is indeed the user's password
        if (!UserUtilities.encryptPassword(password, user.getSalt()).equals(user.getPassword())) {
            passwordStyle = "invalid-input";          
            throw new RuntimeException("That is not your current password. Try again.");
        }
        
        // Check that the new and confirm new passwords are the same
        if (!newPassword.equals(newPasswordConfirm)) {
            newPasswordStyle =  "invalid-input";
            newPasswordConfirmStyle = "invalid-input";
            throw new RuntimeException("The new and confirm password fields do not match.");
        }
        
    }
    
    private void addErrorMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
    }
    
    private void addInfoMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }    
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        UsersFacade uf = new UsersFacade();
        
        // Get the current user from the database
        try {
            user = uf.findById(navigationBean.getCurrentUserId());
        } catch (NoResultException nre) {
            log.error("User id not found.");
            addErrorMessage("An error occured while loading the user information.");
            return;            
        } catch (Exception e) {
            log.error("Database error: " + e.getMessage(), e);
            addErrorMessage("An error occured while loading the user information.");
            return;            
        }
        
        username = user.getUsername();
        passwordStyle = "valid-input";
        newPasswordStyle = "valid-input";
        newPasswordConfirmStyle = "valid-input";
    } 
    
    public void form2Data() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        UsersFacade uf = new UsersFacade();
        Users user = null;
        
        // Find the user
        try {
            user = uf.findById(navigationBean.getCurrentUserId());
        } catch (NoResultException nre) {
            log.error("The user id was not found.", nre);
            addErrorMessage("An error occurred while trying to save the user information.");
            return;
        } catch (Exception e) {
            log.error("The user id was not found.", e);
            addErrorMessage("An error occurred while trying to save the user information.");
            return;
        }         
        
        // Validate form input
        try {
            validateForm();
        } catch(Exception e) {
            log.info("Change Password Validation Error: " + e.getMessage());
            addErrorMessage(e.getMessage());
            return;
        }
        
        // Update entity
        user.setPassword(UserUtilities.encryptPassword(newPassword, user.getSalt()));
        user.setUpdatedAt(new Date());
        user.setUpdatedBy(navigationBean.getCurrentUserId());
        
        // Save
        try {
            uf.save(user);
        } catch(Exception e) {
            log.error("Database Error: ", e);
            addErrorMessage("An error occurred while trying to save the user information.");
        }
        addInfoMessage("The password was successfully updated.");
        // Return
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Manager")) {
            // Navigate back to store details
            NavigationHandler navigator = context.getApplication().getNavigationHandler();
            navigator.handleNavigation(context, null, "pretty:store");
        } else {
            // Navigate back to admin
            NavigationHandler navigator = context.getApplication().getNavigationHandler();
            navigator.handleNavigation(context, null, "pretty:admin");
        }
    } 
    
    public void cancel() {
        FacesContext context = FacesContext.getCurrentInstance();
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Manager")) {
            // Navigate back to store details
            NavigationHandler navigator = context.getApplication().getNavigationHandler();
            navigator.handleNavigation(context, null, "pretty:store");
        } else {
            // Navigate back to admin
            NavigationHandler navigator = context.getApplication().getNavigationHandler();
            navigator.handleNavigation(context, null, "pretty:admin");
        }        
    }
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordStyle() {
        return passwordStyle;
    }

    public void setPasswordStyle(String passwordStyle) {
        this.passwordStyle = passwordStyle;
    }

    public String getNewPasswordConfirm() {
        return newPasswordConfirm;
    }

    public void setNewPasswordConfirm(String newPasswordConfirm) {
        this.newPasswordConfirm = newPasswordConfirm;
    }

    public String getNewPasswordConfirmStyle() {
        return newPasswordConfirmStyle;
    }

    public void setNewPasswordConfirmStyle(String newPasswordConfirmStyle) {
        this.newPasswordConfirmStyle = newPasswordConfirmStyle;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getNewPasswordStyle() {
        return newPasswordStyle;
    }

    public void setNewPasswordStyle(String newPasswordStyle) {
        this.newPasswordStyle = newPasswordStyle;
    }

}
