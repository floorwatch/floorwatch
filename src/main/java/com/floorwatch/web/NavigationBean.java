package com.floorwatch.web;

import com.floorwatch.entities.Users;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;

/**
 * <H3>NavigationBean</H3>
 *
 * ManagedBean for the FloorWatch navigation bar
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="navigationBean")
@SessionScoped
public class NavigationBean {
    
    private int mainSelectedIndex;
    private Integer currentUserId;
    private static Logger log = Logger.getLogger(NavigationBean.class);

    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create NavigationBean
     *
     * @author  Dale Davis
     * @date    10/5/2015
     */
    public NavigationBean() {
        mainSelectedIndex = 0;
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void adminInitialize() {
        FacesContext context = FacesContext.getCurrentInstance();
        // Skip ajax requests.
        if (context.getPartialViewContext().isAjaxRequest()) { 
            return; 
        }
        // If the user is coming from another page, reset all indices
        if (mainSelectedIndex != 0) {
            mainSelectedIndex = 0;
        }    
    }    
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/
    
    public void logout() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        NavigationHandler navigator = context.getApplication().getNavigationHandler();
        HttpServletRequest request = (HttpServletRequest) context.getExternalContext().getRequest();        
        SecurityUtils.getSubject().logout();
        request.getSession().invalidate();        
        navigator.handleNavigation(context, null, "pretty:login");
    }     
    
    /* ===================================================
     * FACES ACTION METHODS
     * ===================================================*/        
    
    /**
     * Action method for clicking on the Adminstration tab
     * 
     * @return  String - the Faces navigation
     * 
     * @author  Dale Davis
     * @date    10/6/2015
     */    
    public String admin() {
        setMainSelectedIndex(0);
        return "pretty:admin";
    }    
     
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public int getMainSelectedIndex() {
        return mainSelectedIndex;
    }

    public void setMainSelectedIndex(int mainSelectedIndex) {
        this.mainSelectedIndex = mainSelectedIndex;
    }

    public Integer getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(Integer currentUserId) {
        this.currentUserId = currentUserId;
    }

}
