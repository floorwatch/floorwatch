package com.floorwatch.web;

import com.floorwatch.common.converters.UserConverter;
import com.floorwatch.common.pojos.SimpleUser;
import com.floorwatch.entities.Stores;
import com.floorwatch.entities.UserStores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.UsersFacade;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * <H3>UserBean</H3>
 *
 * ManagedBean for the FloorWatch user
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="userBean")
@ViewScoped
public class UserBean {
    
    private List<SimpleUser> users;
    private String newPath;
    private String basePath;    
    private static Logger log = Logger.getLogger(UserBean.class);

    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create UserBean
     *
     * @author  Dale Davis
     * @date    10/12/2015
     */
    public UserBean() {
        basePath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "user";
        newPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/admin/user/new";
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        // Initialize
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        UsersFacade uf = new UsersFacade();
        List<Users> dbUsers = new ArrayList<Users>();
        users = new ArrayList<SimpleUser>();
        // If the user is an admin, show all user 
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            try {
                dbUsers = uf.findAll();
            } catch(Exception e) {
            }
        }
        // If the user is corporate, show only applicable users
        if (currentUser.hasRole("Corporate")) {        
            Users user = null;
            Integer currentUserId = navigationBean.getCurrentUserId();
            // Update the user
            try {
                user = uf.findById(currentUserId);
            } catch(NoResultException nre) {
            } catch(Exception e) {}
            List<Stores> stores = user.getUserCompaniesList().get(0).getCompanyId().getStoresList();
            for (Stores store : stores) {
                List<UserStores> userStores = store.getUserStoresList();
                for (UserStores userStore : userStores) {
                    dbUsers.add(userStore.getUserId());
                }
            }
        }        
        for (Users dbUser : dbUsers) {
            SimpleUser user = UserConverter.toPojo(dbUser);
            users.add(user);
        }
    } 
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/    
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public List<SimpleUser> getUsers() {
        return users;
    }

    public void setUsers(List<SimpleUser> users) {
        this.users = users;
    }
    
    public String getNetwork(SimpleUser simpleUser) {
        if (simpleUser.getNetworks() != null && !simpleUser.getNetworks().isEmpty()) {
            return simpleUser.getNetworks().get(0).getDescription();
        }
        return null;
    }
    
    public String getUid(SimpleUser simpleUser) {
        if (simpleUser.getNetworks() != null && !simpleUser.getNetworks().isEmpty()) {
            return simpleUser.getNetworks().get(0).getUid();
        }
        return null;
    }
    
    public String getRole(SimpleUser simpleUser) {
        return simpleUser.getRoles().get(0).getDescription();
    }
    
    public String getProfilePictureUrl(SimpleUser simpleUser) {
        if (simpleUser.getNetworks() != null && !simpleUser.getNetworks().isEmpty()) {
            return "/ProfilePictureServlet?u=" + simpleUser.getId() + "&n=" + getNetwork(simpleUser);
        }
        return null;
    }
    
    public String getStoreAddress(SimpleUser simpleUser) {
        if (simpleUser.getStores() != null && !simpleUser.getStores().isEmpty()) {
            return simpleUser.getStores().get(0).getAddress1() + " " + simpleUser.getStores().get(0).getAddress1()
                    + ", " + simpleUser.getStores().get(0).getCity() + ", " + simpleUser.getStores().get(0).getState();
        }        
        return null;
    }  

    public String getNewPath() {
        return newPath;
    }

    public void setNewPath(String newPath) {
        this.newPath = newPath;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
    
    public String getEditUrl(SimpleUser user) {
        String editPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                + "/admin/user/" + user.getId();
        return editPath;
    }    
}
