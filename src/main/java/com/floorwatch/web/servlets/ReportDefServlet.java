package com.floorwatch.web.servlets;

import com.floorwatch.entities.Reports;
import com.floorwatch.entities.manager.ReportsFacade;
import java.io.IOException;
import java.io.PrintWriter;
import javax.persistence.NoResultException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <H3>ReportDefServlet</H3>
 *
 * Servlet to stream report definitions
 *
 * This file contains proprietary information of Floorwatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

public class ReportDefServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Inheritance of the init method
     * 
     * @param   ServletConfig - The servlet config object
     *
     * @exception ServletException - If a servlet error occurs
     * 
     * @author  Dale Davis
     * @date    11/14/2015
     */    
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
    }

    /**
     * Inheritance of the doGet method.  Will deliver an image with the
     * correct mime type 
     * 
     * @param   ServletRequest - The servlet request we are processing
     * @param   ServletResponse - The servlet response we are creating
     * @param   FilterChain - The filter chain we are processing
     *
     * @exception IOException - If an input/output error occurs
     * @exception ServletException - If a servlet error occurs
     * 
     * @author  Dale Davis
     * @date    11/14/2015
     */    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Initialize needed classes
        ReportsFacade rf = new ReportsFacade();
        Reports report = null;
        String json = null;
        
        // Get the request parameters
        String reportId = request.getParameter("i");
        
        // Get the report
        try {
            report = rf.findById(Integer.parseInt(reportId));
        } catch(NoResultException nre) {
            
        } catch(Exception e) {}

        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print(report.getDescription());
        out.flush();
    }
}
