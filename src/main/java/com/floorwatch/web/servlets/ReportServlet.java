package com.floorwatch.web.servlets;

import com.floorwatch.common.converters.FlareConverter;
import com.floorwatch.common.pojos.SimpleFlare;
import com.floorwatch.entities.Flares;
import com.floorwatch.entities.manager.FlaresFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.primefaces.json.JSONArray;

/**
 * <H3>ReportServlet</H3>
 *
 * Servlet to stream report json
 *
 * This file contains proprietary information of Floorwatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

public class ReportServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Inheritance of the init method
     * 
     * @param   ServletConfig - The servlet config object
     *
     * @exception ServletException - If a servlet error occurs
     * 
     * @author  Dale Davis
     * @date    10/29/2015
     */    
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
    }

    /**
     * Inheritance of the doGet method.  Will deliver an image with the
     * correct mime type 
     * 
     * @param   ServletRequest - The servlet request we are processing
     * @param   ServletResponse - The servlet response we are creating
     * @param   FilterChain - The filter chain we are processing
     *
     * @exception IOException - If an input/output error occurs
     * @exception ServletException - If a servlet error occurs
     * 
     * @author  Dale Davis
     * @date    10/29/2015
     */    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Initialize needed classes
        FlaresFacade ff = new FlaresFacade();
        String json = null;
        
        // Get the request parameters
        String reportGroup = request.getParameter("g");
        
        if (reportGroup.equalsIgnoreCase("flare")) {
            String reportType = request.getParameter("t");
            
            // Get the company
            Integer companyId = null;
            if (request.getParameter("c") != null) {
                companyId = Integer.parseInt(request.getParameter("c"));
            }    
            if (reportType.equalsIgnoreCase("resolved")) {
                List<Flares> resolvedFlares = new ArrayList<Flares>();
                if (companyId == 0) {
                    resolvedFlares = ff.findAllResolved();
                } else {
                    resolvedFlares = ff.findResolvedByCompany(companyId);
                }    
                List<SimpleFlare> simpleFlares = new ArrayList<SimpleFlare>();
                for (Flares flare : resolvedFlares) {
                    SimpleFlare simpleFlare = FlareConverter.toPojo(flare);
                    simpleFlares.add(simpleFlare);
                }
                // Create flat JSON
                JSONArray jsonFlares = new JSONArray();
                for (SimpleFlare simpleFlare : simpleFlares) {
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
                    
                    
                    JSONObject jsonFlare = new JSONObject();
                    jsonFlare.put("company_name", simpleFlare.getStore().getCompany().getDescription());
                    jsonFlare.put("store_description", simpleFlare.getStore().getDescription());
                    jsonFlare.put("store_address_1", simpleFlare.getStore().getAddress1());
                    jsonFlare.put("store_address_2", simpleFlare.getStore().getAddress2() != null ? simpleFlare.getStore().getAddress2() : "");
                    jsonFlare.put("store_city", simpleFlare.getStore().getCity());
                    jsonFlare.put("store_state", simpleFlare.getStore().getState());
                    jsonFlare.put("store_postal_code", simpleFlare.getStore().getPostalCode());
                    jsonFlare.put("store_country", simpleFlare.getStore().getCountry());
                    jsonFlare.put("customer_username", simpleFlare.getCustomerUser().getUserName());
                    jsonFlare.put("customer_email_address", simpleFlare.getCustomerUser().getEmailAddress());
                    jsonFlare.put("customer_first_name", simpleFlare.getCustomerUser().getFirstName());
                    jsonFlare.put("customer_last_name", simpleFlare.getCustomerUser().getLastName());
                    jsonFlare.put("manager_username", simpleFlare.getManagerUser().getUserName());
                    jsonFlare.put("manager_first_name", simpleFlare.getManagerUser().getFirstName());
                    jsonFlare.put("manager_last_name", simpleFlare.getManagerUser().getLastName());
                    Date createdAt = new Date();
                    createdAt.setTime(simpleFlare.getCreatedAt());
                    jsonFlare.put("created_at", sdf.format(createdAt));
                    jsonFlare.put("customer_text", simpleFlare.getCustomerText());
                    jsonFlare.put("manager_text", simpleFlare.getManagerText());
                    jsonFlare.put("customer_followup_text", simpleFlare.getCustomerFollowupText());
                    jsonFlare.put("manager_followup_text", simpleFlare.getManagerFollowupText());
                    jsonFlare.put("resolved", 1);
                    jsonFlare.put("resolved_value", 1);
                    jsonFlare.put("resolved_text", "Resolved");
                    Date resolvedAt = new Date();
                    resolvedAt.setTime(simpleFlare.getResolvedAt());
                    jsonFlare.put("resolved_at", sdf.format(resolvedAt));
                    jsonFlare.put("feedback_text", simpleFlare.getResolvedText());
                    jsonFlare.put("resolved_stars", simpleFlare.getResolvedStars());
                    jsonFlare.put("resolved_stars_value", 1);
                    jsonFlare.put("resolved_stars_text", simpleFlare.getResolvedStars() + " stars");
                    jsonFlares.put(jsonFlare);                

                    
                    
                    
                    
                    
                }
                json = jsonFlares.toString();
                
                
            } else if (reportType.equalsIgnoreCase("unresolved")) {
                List<Flares> unresolvedFlares = new ArrayList<Flares>();
                if (companyId == 0) {
                    unresolvedFlares = ff.findAllUnresolved();
                } else {
                    unresolvedFlares = ff.findUnresolvedByCompany(companyId);
                }    
                List<SimpleFlare> simpleFlares = new ArrayList<SimpleFlare>();
                for (Flares flare : unresolvedFlares) {
                    SimpleFlare simpleFlare = FlareConverter.toPojo(flare);
                    simpleFlares.add(simpleFlare);
                }
                // Create flat JSON
                JSONArray jsonFlares = new JSONArray();
                
                
                for (SimpleFlare simpleFlare : simpleFlares) {
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
                    
                    
                    JSONObject jsonFlare = new JSONObject();
                    jsonFlare.put("company_name", simpleFlare.getStore().getCompany().getDescription());
                    jsonFlare.put("store_description", simpleFlare.getStore().getDescription());
                    jsonFlare.put("store_address_1", simpleFlare.getStore().getAddress1());
                    jsonFlare.put("store_address_2", simpleFlare.getStore().getAddress2() != null ? simpleFlare.getStore().getAddress2() : "");
                    jsonFlare.put("store_city", simpleFlare.getStore().getCity());
                    jsonFlare.put("store_state", simpleFlare.getStore().getState());
                    jsonFlare.put("store_postal_code", simpleFlare.getStore().getPostalCode());
                    jsonFlare.put("store_country", simpleFlare.getStore().getCountry());
                    jsonFlare.put("customer_username", simpleFlare.getCustomerUser().getUserName());
                    jsonFlare.put("customer_email_address", simpleFlare.getCustomerUser().getEmailAddress());
                    jsonFlare.put("customer_first_name", simpleFlare.getCustomerUser().getFirstName());
                    jsonFlare.put("customer_last_name", simpleFlare.getCustomerUser().getLastName());
                    if (simpleFlare.getManagerUser() != null) {
                        jsonFlare.put("manager_username", simpleFlare.getManagerUser().getUserName());
                        jsonFlare.put("manager_first_name", simpleFlare.getManagerUser().getFirstName());
                        jsonFlare.put("manager_last_name", simpleFlare.getManagerUser().getLastName());
                    }    
                    Date createdAt = new Date();
                    createdAt.setTime(simpleFlare.getCreatedAt());
                    jsonFlare.put("created_at", sdf.format(createdAt));
                    jsonFlare.put("customer_text", simpleFlare.getCustomerText());
                    if (simpleFlare.getManagerText() != null) {
                        jsonFlare.put("manager_text", simpleFlare.getManagerText());
                    }
                    if (simpleFlare.getCustomerFollowupText() != null) {
                        jsonFlare.put("customer_followup_text", simpleFlare.getCustomerFollowupText());
                    }
                    if (simpleFlare.getManagerFollowupText() != null) {
                        jsonFlare.put("manager_followup_text", simpleFlare.getManagerFollowupText());
                    }
                    if (simpleFlare.getResolvedAt() != null) {
                        jsonFlare.put("resolved", 1);
                        jsonFlare.put("resolved_value", 1);
                        jsonFlare.put("resolved_text", "Resolved");                        
                        Date resolvedAt = new Date();
                        resolvedAt.setTime(simpleFlare.getResolvedAt());
                        jsonFlare.put("resolved_at", sdf.format(resolvedAt));
                        jsonFlare.put("feedback_text", simpleFlare.getResolvedText());
                        jsonFlare.put("resolved_stars", simpleFlare.getResolvedStars());
                    } else {
                        jsonFlare.put("resolved", 0);
                        jsonFlare.put("resolved_value", 1);
                        jsonFlare.put("resolved_text", "Unresolved");
                    }   
                    jsonFlares.put(jsonFlare);                

                    
                    
                    
                    
                    
                }                
                
                
                json = jsonFlares.toString();
                
            } else if (reportType.equalsIgnoreCase("all")) {
                List<Flares> allFlares = new ArrayList<Flares>();
                if (companyId == 0) {
                    allFlares = ff.findAll();
                } else {
                    allFlares = ff.findAllByCompany(companyId);
                }    
                List<SimpleFlare> simpleFlares = new ArrayList<SimpleFlare>();
                for (Flares flare : allFlares) {
                    SimpleFlare simpleFlare = FlareConverter.toPojo(flare);
                    simpleFlares.add(simpleFlare);
                }
                // Create flat JSON
                JSONArray jsonFlares = new JSONArray();
                
                
                for (SimpleFlare simpleFlare : simpleFlares) {
                    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm");
                    
                    
                    JSONObject jsonFlare = new JSONObject();
                    jsonFlare.put("company_name", simpleFlare.getStore().getCompany().getDescription());
                    jsonFlare.put("store_description", simpleFlare.getStore().getDescription());
                    jsonFlare.put("store_address_1", simpleFlare.getStore().getAddress1());
                    jsonFlare.put("store_address_2", simpleFlare.getStore().getAddress2() != null ? simpleFlare.getStore().getAddress2() : "");
                    jsonFlare.put("store_city", simpleFlare.getStore().getCity());
                    jsonFlare.put("store_state", simpleFlare.getStore().getState());
                    jsonFlare.put("store_postal_code", simpleFlare.getStore().getPostalCode());
                    jsonFlare.put("store_country", simpleFlare.getStore().getCountry());
                    jsonFlare.put("customer_username", simpleFlare.getCustomerUser().getUserName());
                    jsonFlare.put("customer_email_address", simpleFlare.getCustomerUser().getEmailAddress());
                    jsonFlare.put("customer_first_name", simpleFlare.getCustomerUser().getFirstName());
                    jsonFlare.put("customer_last_name", simpleFlare.getCustomerUser().getLastName());
                    if (simpleFlare.getManagerUser() != null) {
                        jsonFlare.put("manager_username", simpleFlare.getManagerUser().getUserName());
                        jsonFlare.put("manager_first_name", simpleFlare.getManagerUser().getFirstName());
                        jsonFlare.put("manager_last_name", simpleFlare.getManagerUser().getLastName());
                    }    
                    Date createdAt = new Date();
                    createdAt.setTime(simpleFlare.getCreatedAt());
                    jsonFlare.put("created_at", sdf.format(createdAt));
                    jsonFlare.put("customer_text", simpleFlare.getCustomerText());
                    if (simpleFlare.getManagerText() != null) {
                        jsonFlare.put("manager_text", simpleFlare.getManagerText());
                    }
                    if (simpleFlare.getCustomerFollowupText() != null) {
                        jsonFlare.put("customer_followup_text", simpleFlare.getCustomerFollowupText());
                    }
                    if (simpleFlare.getManagerFollowupText() != null) {
                        jsonFlare.put("manager_followup_text", simpleFlare.getManagerFollowupText());
                    }
                    if (simpleFlare.getResolvedAt() != null) {
                        jsonFlare.put("resolved", 1);
                        jsonFlare.put("resolved_value", 1);
                        jsonFlare.put("resolved_text", "Resolved");
                        Date resolvedAt = new Date();
                        resolvedAt.setTime(simpleFlare.getResolvedAt());                       
                        jsonFlare.put("resolved_at", sdf.format(resolvedAt));
                        jsonFlare.put("feedback_text", simpleFlare.getResolvedText());
                        jsonFlare.put("resolved_stars", simpleFlare.getResolvedStars());
                    } else {
                        jsonFlare.put("resolved", 0);
                        jsonFlare.put("resolved_value", 1);
                        jsonFlare.put("resolved_text", "Unresolved");
                    }   
                    jsonFlares.put(jsonFlare);                

                    
                    
                    
                    
                    
                }                
                
                
                json = jsonFlares.toString();
                
            } else if (reportType.equalsIgnoreCase("schema")) {
                // Create flat JSON
                LinkedHashMap fieldMap = new LinkedHashMap();
                JSONArray fields = new JSONArray();
                JSONObject field = new JSONObject();
                field.put("name", "company_name");
                field.put("type", "string");
                fields.put(field);
                field = new JSONObject();
                field.put("name", "store_description");
                field.put("type", "string");
                fields.put(field);
                
                field = new JSONObject();
                field.put("name", "store_address_1");
                field.put("type", "string");
                fields.put(field);
                
                
                
                field = new JSONObject();
                field.put("name", "store_address_2");
                field.put("type", "string");
                fields.put(field);
                
                
                
                field = new JSONObject();
                field.put("name", "store_city");
                field.put("type", "string");
                fields.put(field);
                
                
                
                field = new JSONObject();
                field.put("name", "store_state");
                field.put("type", "string");
                fields.put(field);
                
                
                
                field = new JSONObject();
                field.put("name", "store_postal_code");
                field.put("type", "string");
                fields.put(field);
                
                
                
                field = new JSONObject();
                field.put("name", "store_country");
                field.put("type", "string");
                fields.put(field);
                
                
                
                field = new JSONObject();
                field.put("name", "customer_username");
                field.put("type", "string");
                fields.put(field);
                
                
                field = new JSONObject();
                field.put("name", "customer_email_address");
                field.put("type", "string");
                fields.put(field); 
                
                
                field = new JSONObject();
                field.put("name", "customer_first_name");
                field.put("type", "string");
                fields.put(field);                
                
                
                
                
                field = new JSONObject();
                field.put("name", "customer_last_name");
                field.put("type", "string");
                fields.put(field);               
                
                
                
                
                field = new JSONObject();
                field.put("name", "manager_username");
                field.put("type", "string");
                fields.put(field);        
                
                
                field = new JSONObject();
                field.put("name", "manager_first_name");
                field.put("type", "string");
                fields.put(field);               
                
                
                field = new JSONObject();
                field.put("name", "customer_last_name");
                field.put("type", "string");
                fields.put(field);               
                
                
                
                field = new JSONObject();
                field.put("name", "created_at");
                field.put("type", "date");
                field.put("dateFormat", "MM/DD/YYYY HH:MM");
                fields.put(field);                
                
                
                
                field = new JSONObject();
                field.put("name", "customer_text");
                field.put("type", "string");
                fields.put(field);              
                
                
                
                field = new JSONObject();
                field.put("name", "manager_text");
                field.put("type", "string");
                fields.put(field);             
                
                
                
                field = new JSONObject();
                field.put("name", "customer_followup_text");
                field.put("type", "string");
                fields.put(field);                
                
                
                field = new JSONObject();
                field.put("name", "manager_followup_text");
                field.put("type", "string");
                fields.put(field);                
                
                
                field = new JSONObject();
                field.put("name", "customer_last_name");
                field.put("type", "string");
                fields.put(field);                
                
                
                field = new JSONObject();
                field.put("name", "resolved");
                field.put("type", "number");
                fields.put(field);                
                
                
                field = new JSONObject();
                field.put("name", "resolved_value");
                field.put("type", "number");
                fields.put(field);                
                
                
                field = new JSONObject();
                field.put("name", "resolved_text");
                field.put("type", "string");
                fields.put(field);                
                
                
                field = new JSONObject();
                field.put("name", "resolved_at");
                field.put("type", "date");
                field.put("dateFormat", "MM/DD/YYYY HH:MM");
                fields.put(field);
                
                
                
                field = new JSONObject();
                field.put("name", "feedback_text");
                field.put("type", "string");
                fields.put(field);
                
                
                field = new JSONObject();
                field.put("name", "resolved_stars");
                field.put("type", "number");
                fields.put(field);
                
                               
                field = new JSONObject();
                field.put("name", "resolved_stars_value");
                field.put("type", "number");
                fields.put(field);                
                
                
                
                field = new JSONObject();
                field.put("name", "resolved_stars_text");
                field.put("type", "string");
                fields.put(field);                

                
                
                fieldMap.put("fields", fields);
                json = JSONValue.toJSONString(fieldMap); 
            }            
        }
        response.setContentType("application/json");
        PrintWriter out = response.getWriter();
        out.print(json);
        out.flush();
    }
}
