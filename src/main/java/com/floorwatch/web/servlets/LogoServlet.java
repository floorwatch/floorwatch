package com.floorwatch.web.servlets;

import com.floorwatch.entities.Logos;
import com.floorwatch.entities.manager.LogosFacade;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.persistence.NoResultException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

/**
 * <H3>CompanyLogoServlet</H3>
 *
 * Servlet to stream logos
 *
 * This file contains proprietary information of Floorwatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

public class LogoServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    /**
     * Inheritance of the init method
     * 
     * @param   ServletConfig - The servlet config object
     *
     * @exception ServletException - If a servlet error occurs
     * 
     * @author  Dale Davis
     * @date    11/30/2015
     */    
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
    }

    /**
     * Inheritance of the doGet method.  Will deliver an image with the
     * correct mime type 
     * 
     * @param   ServletRequest - The servlet request we are processing
     * @param   ServletResponse - The servlet response we are creating
     * @param   FilterChain - The filter chain we are processing
     *
     * @exception IOException - If an input/output error occurs
     * @exception ServletException - If a servlet error occurs
     * 
     * @author  Dale Davis
     * @date    11/30/2015
     */    
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Initialize needed classes
        LogosFacade lf = new LogosFacade();
        Logos logo = null;
        
        // Get the request parameters
        String logoId = request.getParameter("l");

        // Get the user from the DB
        try {
            logo = lf.findById(Integer.parseInt(logoId));
        } catch (NoResultException nre) {
        } catch (Exception e) {
        }
        // Get the proper profile pic
        String logoImage = logo.getLogo();
                
        byte[] logoImageDecoded = Base64.decodeBase64(logoImage);
        
        response.setContentType("image/jpg");
        InputStream is = new ByteArrayInputStream(logoImageDecoded);;
        IOUtils.copy(is, response.getOutputStream());
    }
}
