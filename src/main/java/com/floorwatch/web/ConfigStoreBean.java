package com.floorwatch.web;

import com.floorwatch.entities.Companies;
import com.floorwatch.entities.Stores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.CompaniesFacade;
import com.floorwatch.entities.manager.StoresFacade;
import com.floorwatch.entities.manager.UsersFacade;
import com.floorwatch.web.util.CountryUtilities;
import com.floorwatch.web.util.StateUtilities;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * <H3>ConfigStoreBean</H3>
 *
 * ManagedBean for the FloorWatch store configuration
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="configStoreBean")
@ViewScoped
public class ConfigStoreBean {
    
    private String description;
    private String descriptionStyle;
    private String address1;
    private String address1Style;
    private String address2;
    private String address2Style;
    private String city;
    private String cityStyle;
    private String state;
    private String stateStyle;
    private Map statesDropdown;
    private String country;
    private String countryStyle;
    private Map countriesDropdown;
    private String postalCode;
    private String postalCodeStyle;
    private String phone;
    private String phoneStyle;
    private String latitude;
    private String latitudeStyle;
    private String longitude;
    private String longitudeStyle;
    private Integer storeId;
    private Integer companyId;
    private Map<String, Integer> companiesDropdown; 
    private String companyStyle;
    private String backUrl;
    private static Logger log = Logger.getLogger(ConfigStoreBean.class);
    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create ConfigStoreBean
     *
     * @author  Dale Davis
     * @date    11/5/2015
     */
    public ConfigStoreBean() {
        FacesContext context = FacesContext.getCurrentInstance();
        // Determine if there is a company id
        if (context.getExternalContext().getRequestParameterMap().get("storeId") != null) {
            try {
                storeId = (Integer) Integer.parseInt((String)context.getExternalContext().getRequestParameterMap().get("storeId"));        
            } catch(NumberFormatException e){
            }    
        }
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            navigationBean.setMainSelectedIndex(1);
        } else {
            navigationBean.setMainSelectedIndex(0);
        }
        backUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                + "/admin";        
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    private void validateForm() {
        // Reset fields
        companyStyle = "valid-input";
        descriptionStyle = "valid-input";
        address1Style = "valid-input";
        address2Style = "valid-input";
        cityStyle = "valid-input";
        stateStyle = "valid-input";
        countryStyle = "valid-input";
        postalCodeStyle = "valid-input";
        phoneStyle = "valid-input";
        latitudeStyle = "valid-input";
        longitudeStyle = "valid-input";        

        // Required fields
        boolean requiredFieldsMissing = false;
        
        // Only check the company if the user is an admin
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            if (companyId == null) {
                companyStyle = "invalid-input";
                requiredFieldsMissing = true;
            }
        }
        if (description == null || description.length() == 0) {
            descriptionStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        if (address1 == null || address1.length() == 0) {
            address1Style = "invalid-input";
            requiredFieldsMissing = true;
        }
        if (city == null || city.length() == 0) {
            cityStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        if (country == null || country.length() == 0) {
            countryStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        if (postalCode == null || postalCode.length() == 0) {
            postalCodeStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        if (latitude == null || latitude.length() == 0) {
            latitudeStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        if (longitude == null || longitude.length() == 0) {
            longitudeStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        if (requiredFieldsMissing) {
            throw new RuntimeException("There are required fields missing.");
        }
    }
    
    private void addErrorMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
    }
    
    private void addInfoMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }    
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        CompaniesFacade cf = new CompaniesFacade();
        StoresFacade sf = new StoresFacade();        
        
        // Create the companies dropdown
        setCompaniesDropdown(new LinkedHashMap<String, Integer>());
        try {
            List<Companies> companies = cf.findAll();
            for (Companies company : companies) {
                getCompaniesDropdown().put(company.getDescription(), company.getId());     
            }
        } catch(Exception e) {}
        
        // If the company exists, get from the database
        if (storeId != null) {
            
            Stores store = null;
            try {
                store = sf.findById(storeId);
            } catch(NoResultException nre) {
            } catch(Exception e) {
            }
            companyId = store.getCompanyId().getId();
            description = store.getDescription();
            address1 = store.getAddress1();
            if (store.getAddress2() != null) {
                address2 = store.getAddress2();
            }
            city = store.getCity();
            if (store.getState() != null) {
                state = store.getState();
            }
            country = store.getCountry();
            postalCode = store.getPostalCode();
            if (store.getPhone() != null) {
                phone = store.getPhone();
            }
            latitude = store.getLatitude().toString();
            longitude = store.getLongitude().toString();
        } 
        companyStyle = "valid-input";
        descriptionStyle = "valid-input";
        address1Style = "valid-input";
        address2Style = "valid-input";
        cityStyle = "valid-input";
        stateStyle = "valid-input";
        countryStyle = "valid-input";
        postalCodeStyle = "valid-input";
        phoneStyle = "valid-input";
        latitudeStyle = "valid-input";
        longitudeStyle = "valid-input";
        statesDropdown = StateUtilities.getStates();
        countriesDropdown = CountryUtilities.getCountries();
    } 
    
    public void form2Data() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        StoresFacade sf = new StoresFacade();
        CompaniesFacade cf = new CompaniesFacade();
        Stores store = null;
        Companies company = null;
        
        // Validate form input
        try {
            validateForm();
        } catch(Exception e) {
            log.info("Config Store Validation Error: " + e.getMessage());
            addErrorMessage(e.getMessage());
            return;
        }
        
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            try {
                company = cf.findById(companyId);
            } catch(NoResultException nre) {
                log.error("Company id not found.");
                addErrorMessage("An error occured while saving the store.");
                return;
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the store.");
                return;                
            }
        } else {     
            // Get the user from the database
            Integer userId = navigationBean.getCurrentUserId();
            UsersFacade uf = new UsersFacade();        
            Users user = null;
            try {
                user = uf.findById(userId);
            } catch(NoResultException nre) {
                log.error("User id not found.");
                addErrorMessage("An error occured while saving the store.");
                return;            
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the store.");
                return;            
            }
            company = user.getUserCompaniesList().get(0).getCompanyId();
        }    
        
        // Get the company from the database if this is an edit
        if (storeId != null) {
            try {
                store = sf.findById(storeId);
            } catch(NoResultException nre) {
                log.error("Store id not found.");
                addErrorMessage("An error occured while saving the store.");
                return;
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the store.");
                return;                
            }
            store.setCompanyId(company);
            store.setDescription(description);
            store.setAddress1(address1);
            store.setAddress2(address2);
            store.setCity(city);
            store.setState(state);
            store.setCountry(country);
            store.setPostalCode(postalCode);
            store.setPhone(phone);
            store.setLatitude(new Double(latitude));
            store.setLongitude(new Double(longitude));
            store.setUpdatedBy(navigationBean.getCurrentUserId());
            store.setUpdatedAt(new Date());
        } else {
            store = new Stores();
            store.setCompanyId(company);
            store.setDescription(description);
            store.setAddress1(address1);
            store.setAddress2(address2);
            store.setCity(city);
            store.setState(state);
            store.setCountry(country);
            store.setPostalCode(postalCode);
            store.setPhone(phone);
            store.setLatitude(new Double(latitude));
            store.setLongitude(new Double(longitude));            
            store.setIsActive(new Short("1"));
            store.setCreatedBy(navigationBean.getCurrentUserId());
            store.setCreatedAt(new Date());
            store.setUpdatedBy(navigationBean.getCurrentUserId());
            store.setUpdatedAt(new Date());
            
        }
        // Save
        try {
            sf.save(store);
        } catch(Exception e) {
            log.error("Database error: " + e.getMessage(), e);
            addErrorMessage("An error occured while saving the store.");
            return;            
        }
        // Navigate back to the admin tab list -> Stores
        if (currentUser.hasRole("Administration")) {
            navigationBean.setMainSelectedIndex(1);
        } else {
            navigationBean.setMainSelectedIndex(0);
        }    
        NavigationHandler navigator = context.getApplication().getNavigationHandler();
        navigator.handleNavigation(context, null, "pretty:admin");
    }    
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionStyle() {
        return descriptionStyle;
    }

    public void setDescriptionStyle(String descriptionStyle) {
        this.descriptionStyle = descriptionStyle;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress1Style() {
        return address1Style;
    }

    public void setAddress1Style(String address1Style) {
        this.address1Style = address1Style;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress2Style() {
        return address2Style;
    }

    public void setAddress2Style(String address2Style) {
        this.address2Style = address2Style;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCityStyle() {
        return cityStyle;
    }

    public void setCityStyle(String cityStyle) {
        this.cityStyle = cityStyle;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateStyle() {
        return stateStyle;
    }

    public void setStateStyle(String stateStyle) {
        this.stateStyle = stateStyle;
    }

    public Map getStatesDropdown() {
        return statesDropdown;
    }

    public void setStatesDropdown(Map statesDropdown) {
        this.statesDropdown = statesDropdown;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCountryStyle() {
        return countryStyle;
    }

    public void setCountryStyle(String countryStyle) {
        this.countryStyle = countryStyle;
    }

    public Map getCountriesDropdown() {
        return countriesDropdown;
    }

    public void setCountriesDropdown(Map countriesDropdown) {
        this.countriesDropdown = countriesDropdown;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getPostalCodeStyle() {
        return postalCodeStyle;
    }

    public void setPostalCodeStyle(String postalCodeStyle) {
        this.postalCodeStyle = postalCodeStyle;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLatitudeStyle() {
        return latitudeStyle;
    }

    public void setLatitudeStyle(String latitudeStyle) {
        this.latitudeStyle = latitudeStyle;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLongitudeStyle() {
        return longitudeStyle;
    }

    public void setLongitudeStyle(String longitudeStyle) {
        this.longitudeStyle = longitudeStyle;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Map<String, Integer> getCompaniesDropdown() {
        return companiesDropdown;
    }

    public void setCompaniesDropdown(Map<String, Integer> companiesDropdown) {
        this.companiesDropdown = companiesDropdown;
    }

    public String getCompanyStyle() {
        return companyStyle;
    }

    public void setCompanyStyle(String companyStyle) {
        this.companyStyle = companyStyle;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public void setBackUrl(String backUrl) {
        this.backUrl = backUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneStyle() {
        return phoneStyle;
    }

    public void setPhoneStyle(String phoneStyle) {
        this.phoneStyle = phoneStyle;
    }

}
