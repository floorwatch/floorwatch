package com.floorwatch.web;

import com.floorwatch.entities.Companies;
import com.floorwatch.entities.manager.CompaniesFacade;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 * <H3>CompanyBean</H3>
 *
 * ManagedBean for the FloorWatch companies
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="companyBean")
@ViewScoped
public class CompanyBean {
    
    private List<Companies> companies;
    private String newPath;    
    private static Logger log = Logger.getLogger(CompanyBean.class);

    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create CompanyBean
     *
     * @author  Dale Davis
     * @date    11/5/2015
     */
    public CompanyBean() {
        newPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/admin/company/new";
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        CompaniesFacade cf = new CompaniesFacade();
        try {
            setCompanies(cf.findAll());
        } catch(Exception e) {
            
        }
    } 
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/    
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public List<Companies> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Companies> companies) {
        this.companies = companies;
    }
    
    public String getCompanyLogoUrl(Companies company) {
        return "/CompanyLogoServlet?c=" + company.getId();
    }
    
    public String getEditUrl(Companies company) {
        String editPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                + "/admin/company/" + company.getId();
        return editPath;
    }

    public String getNewPath() {
        return newPath;
    }

    public void setNewPath(String newPath) {
        this.newPath = newPath;
    }
}
