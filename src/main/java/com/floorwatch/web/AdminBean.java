package com.floorwatch.web;

import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.UsersFacade;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.TabChangeEvent;

/**
 * <H3>AdminBean</H3>
 *
 * ManagedBean for the FloorWatch admin
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="adminBean")
@ViewScoped
public class AdminBean {
    
    private enum TABS {

        companyTab, storeTab, userTab, reportTab
    }    
    private String companyDescription;
    private String companyLogoUrl;
    private boolean companyDescriptionRendered;
    private static Logger log = Logger.getLogger(AdminBean.class);

    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create CompanyBean
     *
     * @author  Dale Davis
     * @date    11/5/2015
     */
    public AdminBean() {
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        companyDescriptionRendered = false;
        Subject currentUser = SecurityUtils.getSubject();
        if (!currentUser.hasRole("Administration") && !currentUser.hasRole("Corporate")) {
            try {
                context.getExternalContext().responseSendError(403, "Invalid request.");
                context.responseComplete();
                return;
            } catch (Exception e) {
            }
        }
        if (currentUser.hasRole("Corporate")) {
            UsersFacade uf = new UsersFacade();
            Users user = null;
            Integer userId = navigationBean.getCurrentUserId();
            try {
                user = uf.findById(userId);
            } catch(NoResultException nre) {
            } catch(Exception e) {}
            companyDescription = user.getUserCompaniesList().get(0).getCompanyId().getDescription();
            companyLogoUrl =  "/CompanyLogoServlet?c=" + user.getUserCompaniesList().get(0).getCompanyId().getId();
            companyDescriptionRendered = true;
        }
    } 
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/
    
    public void onChange(TabChangeEvent event) {
        TABS selectedTab = TABS.valueOf(event.getTab().getId());
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        switch (selectedTab) {
            case companyTab:
                navigationBean.setMainSelectedIndex(0);
                break;
            case storeTab:
                navigationBean.setMainSelectedIndex(1);
                break;
            case userTab:
                navigationBean.setMainSelectedIndex(2);
                break;
            case reportTab:
                navigationBean.setMainSelectedIndex(3);
                break;
        }
    }    
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public String getCompanyDescription() {
        return companyDescription;
    }

    public void setCompanyDescription(String companyDescription) {
        this.companyDescription = companyDescription;
    }

    public boolean isCompanyDescriptionRendered() {
        return companyDescriptionRendered;
    }

    public void setCompanyDescriptionRendered(boolean companyDescriptionRendered) {
        this.companyDescriptionRendered = companyDescriptionRendered;
    }

    public String getCompanyLogoUrl() {
        return companyLogoUrl;
    }

    public void setCompanyLogoUrl(String companyLogoUrl) {
        this.companyLogoUrl = companyLogoUrl;
    }

}
