package com.floorwatch.web;

import com.floorwatch.entities.Companies;
import com.floorwatch.entities.Logos;
import com.floorwatch.entities.manager.CompaniesFacade;
import com.floorwatch.entities.manager.LogosFacade;
import java.util.Date;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 * <H3>ConfigCompanyBean</H3>
 *
 * ManagedBean for the FloorWatch company configuration
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="configCompanyBean")
@ViewScoped
public class ConfigCompanyBean {
    
    private Integer companyId;
    private String description;
    private String descriptionStyle;
    private String phone;
    private String phoneStyle;
    
    private String webAddress;
    private String webAddressStyle;
    
    private String facebookAddress;
    private String facebookAddressStyle;    
    
    private String twitterHandle;
    private String twitterHandleStyle;    
    
    private UploadedFile file;
    private Integer logoId;
    private String logoUrl;
    private String backUrl;
    private static Logger log = Logger.getLogger(ConfigCompanyBean.class);
    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create ConfigCompanyBean
     *
     * @author  Dale Davis
     * @date    11/5/2015
     */
    public ConfigCompanyBean() {
        FacesContext context = FacesContext.getCurrentInstance();
        // Determine if there is a company id
        if (context.getExternalContext().getRequestParameterMap().get("companyId") != null) {
            try {
                companyId = (Integer) Integer.parseInt((String)context.getExternalContext().getRequestParameterMap().get("companyId"));        
            } catch(NumberFormatException e){
            }    
        }
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        navigationBean.setMainSelectedIndex(0);
        backUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                + "/admin";
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    private void validateForm() {
        CompaniesFacade cf = new CompaniesFacade();
        Companies company = null;
        
        // Reset fields
        descriptionStyle = "valid-input";
        phoneStyle = "valid-input";
        webAddressStyle = "valid-input";
        facebookAddressStyle = "valid-input";
        twitterHandleStyle = "valid-input";
        
        // Required fields
        if (logoId == null ||
                description == null ||
                description.length() == 0) {
            descriptionStyle = "invalid-input";
            throw new RuntimeException("There are required fields missing.");
        }
        // Company name can not be a duplicate
        try {
            company = cf.findByDescription(description);
        } catch(NoResultException nre) {           
        } catch(Exception e) {}
        if (company != null) {
            // Edit
            if (companyId != null  && company.getId() != companyId) {
                descriptionStyle = "invalid-input";
                throw new RuntimeException("A company with this name has already been created.");
            // New    
            } else if (companyId == null) {
                descriptionStyle = "invalid-input";
                throw new RuntimeException("A company with this name has already been created.");
            }
        }
    }
    
    private void addErrorMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
    }
    
    private void addInfoMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }    
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        // If the company exists, get from the database
        if (companyId != null) {
            CompaniesFacade cf = new CompaniesFacade();
            Companies company = null;
            try {
                company = cf.findById(companyId);
            } catch(NoResultException nre) {
            } catch(Exception e) {
            }
            description = company.getDescription();
            if (company.getPhone() != null) {
                phone = company.getPhone();
            }
            if (company.getWebAddress() != null) {
                webAddress = company.getWebAddress();
            }
            if (company.getFacebookAddress() != null) {
                facebookAddress = company.getFacebookAddress();
            }
            if (company.getTwitterHandle() != null) {
                twitterHandle = company.getTwitterHandle();
            }
            logoId = company.getLogoId().getId();
            logoUrl = "/LogoServlet?l=" + company.getLogoId().getId();            
        }
        descriptionStyle = "valid-input";
        phoneStyle = "valid-input";
        webAddressStyle = "valid-input";        
        facebookAddressStyle = "valid-input";        
        twitterHandleStyle = "valid-input";        
    }
    
    public void form2Data() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        CompaniesFacade cf = new CompaniesFacade();
        LogosFacade lf = new LogosFacade();
        Companies company = null;
        Logos logo = null;
        
        // Validate form input
        try {
            validateForm();
        } catch(Exception e) {
            log.info("Config Company Validation Error: " + e.getMessage());
            addErrorMessage(e.getMessage());
            return;
        }
        
        // Get the logo
        try {
            logo = lf.findById(logoId);
        } catch(NoResultException nre) {
            log.error("Logo id not found.");
            addErrorMessage("An error occured while saving the company.");
            return;
        } catch(Exception e) {
            log.error("Database error: " + e.getMessage(), e);
            addErrorMessage("An error occured while saving the company.");
            return;
        }
        
        // Get the company from the database if this is an edit
        if (companyId != null) {
            try {
                company = cf.findById(companyId);
            } catch(NoResultException nre) {
                log.error("Company id not found.");
                addErrorMessage("An error occured while saving the company.");
                return;
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the company.");
                return;                
            }
            company.setLogoId(logo);
            company.setDescription(description);
            company.setPhone(phone);
            company.setWebAddress(webAddress);
            company.setFacebookAddress(facebookAddress);
            company.setTwitterHandle(twitterHandle);
            company.setUpdatedBy(navigationBean.getCurrentUserId());
            company.setUpdatedAt(new Date());
        } else {
            company = new Companies();
            company.setDescription(description);
            company.setPhone(phone);
            company.setWebAddress(webAddress);
            company.setFacebookAddress(facebookAddress);
            company.setTwitterHandle(twitterHandle);           
            company.setLogoId(logo);
            company.setIsActive(new Short("1"));
            company.setCreatedBy(navigationBean.getCurrentUserId());
            company.setCreatedAt(new Date());
            company.setUpdatedBy(navigationBean.getCurrentUserId());
            company.setUpdatedAt(new Date());
        }
        // Save
        try {
            cf.save(company);
        } catch(Exception e) {
            log.error("Database error: " + e.getMessage(), e);
            addErrorMessage("An error occured while saving the company.");
            return;            
        }
        // Navigate back to the admin tab list -> Companies
        navigationBean.setMainSelectedIndex(0);
        NavigationHandler navigator = context.getApplication().getNavigationHandler();
        navigator.handleNavigation(context, null, "pretty:admin");
    }    
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/    
    
    public void handleFileUpload(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        LogosFacade lf = new LogosFacade();
        Logos logo = new Logos();
        file = event.getFile();
        String encodedString = null;
        try {
            encodedString = Base64.encodeBase64URLSafeString(file.getContents());
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }        
        logo.setLogo(encodedString);
        logo.setCreatedBy(navigationBean.getCurrentUserId());
        logo.setCreatedAt(new Date());
        logo.setUpdatedBy(navigationBean.getCurrentUserId());
        logo.setUpdatedAt(new Date());
        
        try {
            logo = lf.save(logo);
        } catch(Exception e) {
            e.printStackTrace();
            return;
        }
        logoId = logo.getId();
        logoUrl = "/LogoServlet?l=" + logo.getId();
    }        
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionStyle() {
        return descriptionStyle;
    }

    public void setDescriptionStyle(String descriptionStyle) {
        this.descriptionStyle = descriptionStyle;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public Integer getLogoId() {
        return logoId;
    }

    public void setLogoId(Integer logoId) {
        this.logoId = logoId;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public void setBackUrl(String backUrl) {
        this.backUrl = backUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneStyle() {
        return phoneStyle;
    }

    public void setPhoneStyle(String phoneStyle) {
        this.phoneStyle = phoneStyle;
    }

    public String getWebAddress() {
        return webAddress;
    }

    public void setWebAddress(String webAddress) {
        this.webAddress = webAddress;
    }

    public String getWebAddressStyle() {
        return webAddressStyle;
    }

    public void setWebAddressStyle(String webAddressStyle) {
        this.webAddressStyle = webAddressStyle;
    }

    public String getFacebookAddress() {
        return facebookAddress;
    }

    public void setFacebookAddress(String facebookAddress) {
        this.facebookAddress = facebookAddress;
    }

    public String getFacebookAddressStyle() {
        return facebookAddressStyle;
    }

    public void setFacebookAddressStyle(String facebookAddressStyle) {
        this.facebookAddressStyle = facebookAddressStyle;
    }

    public String getTwitterHandle() {
        return twitterHandle;
    }

    public void setTwitterHandle(String twitterHandle) {
        this.twitterHandle = twitterHandle;
    }

    public String getTwitterHandleStyle() {
        return twitterHandleStyle;
    }

    public void setTwitterHandleStyle(String twitterHandleStyle) {
        this.twitterHandleStyle = twitterHandleStyle;
    }
}
