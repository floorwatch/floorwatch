package com.floorwatch.web;

import com.floorwatch.entities.Companies;
import com.floorwatch.entities.Reports;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.CompaniesFacade;
import com.floorwatch.entities.manager.ReportsFacade;
import com.floorwatch.entities.manager.UsersFacade;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * <H3>ReportsBean</H3>
 *
 * ManagedBean for the FloorWatch reports
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="reportBean")
@ViewScoped
public class ReportBean {
    
    private Integer companyId;
    private String reportDesignerJavascript;
    private String reportDisplayJavascript;
    private Users user;
    private Map<String, Integer> reportsDropdown;
    private Integer reportId;
    private static Logger log = Logger.getLogger(ReportBean.class);

    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create CompanyBean
     *
     * @author  Dale Davis
     * @date    11/5/2015
     */
    public ReportBean() {
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class); 
        UsersFacade uf = new UsersFacade();
        CompaniesFacade cf = new CompaniesFacade();
        ReportsFacade rf = new ReportsFacade();
        List<Companies> companies = new ArrayList<Companies>();
        List<Reports> reports = new ArrayList<Reports>();
        setReportsDropdown(new LinkedHashMap<String, Integer>());
        Subject currentUser = SecurityUtils.getSubject();
        try {
            setUser(uf.findById(navigationBean.getCurrentUserId()));
        } catch (NoResultException nre) {
        } catch (Exception e) {
        }
        if (currentUser.hasRole("Administration")) {
            companyId = 0;
        } else {
            companyId = getUser().getUserCompaniesList().get(0).getId();
        }
        // Get all the companies if company id is admin
        if (currentUser.hasRole("Administration")) {
            try {
                companies = cf.findAll();
            } catch(Exception e) {}
        } else {
            companies.add(getUser().getUserCompaniesList().get(0).getCompanyId());
        }
        
        // Get all reports
        try {
            reports = rf.findAll();
        } catch(Exception e) {}
        
        reportDesignerJavascript = "";
        for (Reports report : reports) {
            reportDesignerJavascript +=  "$.get(\"/fw/ReportDefServlet?i=" + report.getId() + "\", function(report" + report.getId() + ") {\n";
        }    
        reportDesignerJavascript += "                        var designer = new jsreports.Designer({\n"
            + "                            embedded: true,\n"
            + "                            container: $(\"#report-div\"),\n"
            + "                            showDownloadButton: true,\n"
            + "                            showSaveButton: " + currentUser.hasRole("Administration") + ",\n"
            + "                            showConfigPanel: false,\n"
            + "                            images: [\n";
        
        for (Companies company : companies) {
            reportDesignerJavascript += "                            {\n"
                    + "                                name: \"Company Logo\",\n"
                    + "                                url: \"/fw/CompanyLogoServlet?c=" + company.getId() + "\"\n"
                    + "                            },\n";
        }        

        reportDesignerJavascript += "                            ],\n"                
            + "                            data_sources: [\n"
            + "                                {\"id\": \"resolved-flares\",\n"
            + "                                    \"name\": \"Resolved Flares\",\n"
            + "                                    \"url\": \"/fw/ReportServlet?g=flare&t=resolved&c=" + companyId + "\",\n"
            + "                                    \"schema_url\": \"/fw/ReportServlet?g=flare&t=schema\"},\n"
            + "                                {\"id\": \"unresolved-flares\",\n"
            + "                                    \"name\": \"Unresolved Flares\",\n"
            + "                                    \"url\": \"/fw/ReportServlet?g=flare&t=unresolved&c=" + companyId + "\",\n"
            + "                                    \"schema_url\": \"/fw/ReportServlet?g=flare&t=schema\"},\n"
            + "                                {\"id\": \"all-flares\",\n"
            + "                                    \"name\": \"All Flares\",\n"
            + "                                    \"url\": \"/fw/ReportServlet?g=flare&t=all&c=" + companyId + "\",\n"
            + "                                    \"schema_url\": \"/fw/ReportServlet?g=flare&t=schema\"}],\n"
            + "                            plugins: [{\n"
            + "                                    pluginType: 'ReportList',\n"
            + "                                         reports: [";
        for (Reports report : reports) {        
            reportDesignerJavascript += "report" + report.getId() + ",";
        }
        reportDesignerJavascript = reportDesignerJavascript.replaceAll(",$", "");
        reportDesignerJavascript += "],\n"
            + "                                    allowNew: " + currentUser.hasRole("Administration") + ",\n"
            + "                                    allowDelete: false,\n"
            + "                                    selectorLabel: 'Choose a report:',\n"
            + "                                    listeners: {\n"
            + "                                        \"createreport\": function (evt, report) {\n"
            + "                                            report.title = window.prompt('Enter report title:', report.title);\n"
            + "                                        },\n"
            + "                                        \"deletereport\": function (evt, report) {\n"
            + "                                            // TODO handle report deletion here\n"
            + "                                        }\n"
            + "                                    }\n"
            + "                                }]\n"
            + "                        });\n"
            + "                        $(designer).on(\"save\", function (evt, reportdef) {\n"
            + "                            saveReport([{name: 'reportDef', value: reportdef}]);\n"
            + "                        });\n";
        for (Reports report : reports) {
            reportDesignerJavascript +=  "                        });\n";
        }
        
        // Reports dropdown
        for (Reports report : reports) {
            getReportsDropdown().put(report.getName(), report.getId());
        }
        
    } 
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/
    
    public void saveReport() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        ReportsFacade rf = new ReportsFacade();
        Reports report = null;
        JSONParser parser = new JSONParser();        
        String reportDef = (String)FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("reportDef");
        String name = null;
        JSONObject jsonObject = null;
        try {
            Object obj = parser.parse(reportDef);
            jsonObject = (JSONObject) obj;
            name = (String) jsonObject.get("title");
            Date now = new Date();
            jsonObject.put("id", navigationBean.getCurrentUserId() + "-" + now.getTime());            
        } catch(ParseException pe) {
            pe.printStackTrace();
        }
        try {
            report = rf.findByName(name);
        } catch(NoResultException nre) {
        } catch(Exception e) {}
        if (report == null) {
            report = new Reports();
            report.setName(name);
            report.setDescription(jsonObject.toJSONString());
            report.setCreatedAt(new Date());
            report.setCreatedBy(user.getId());
            report.setUpdatedAt(new Date());
            report.setUpdatedBy(user.getId());
        } else {
            report.setDescription(reportDef);
            report.setUpdatedAt(new Date());
            report.setUpdatedBy(user.getId());            
        }    
        try {
            rf.save(report);
        } catch(Exception e) {}
        data2Form();
    }
    
    public void displayReport() {
        ReportsFacade rf = new ReportsFacade();
        Reports report = null;
        if (getReportId() == 0) {
            return;
        }
        try {
            report = rf.findById(getReportId());
        } catch(NoResultException nre) {
        } catch (Exception e) {}
        reportDisplayJavascript = "                jsreports.render({\n"
            + "                    datasets: [\n"
            + "                        {\"id\": \"resolved-flares\",\n"
            + "                            \"name\": \"Resolved Flares\",\n"
            + "                            \"url\": \"/fw/ReportServlet?g=flare&t=resolved&c=" + companyId + "\",\n"
            + "                            \"schema_url\": \"/fw/ReportServlet?g=flare&t=schema\"},\n"
            + "                        {\"id\": \"unresolved-flares\",\n"
            + "                            \"name\": \"Unresolved Flares\",\n"
            + "                            \"url\": \"/fw/ReportServlet?g=flare&t=unresolved&c=" + companyId + "\",\n"
            + "                            \"schema_url\": \"/fw/ReportServlet?g=flare&t=schema\"},\n"
            + "                        {\"id\": \"all-flares\",\n"
            + "                            \"name\": \"All Flares\",\n"
            + "                            \"url\": \"/fw/ReportServlet?g=flare&t=all&c=" + companyId + "\",\n"
            + "                            \"schema_url\": \"/fw/ReportServlet?g=flare&t=schema\"}],\n"
            + "                    report_def: " + report.getDescription() + ",\n"
            + "                    target: $(\"#report-display-div\")\n"
            + "                });\n";        
    }
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public String getReportDesignerJavascript() {
        return reportDesignerJavascript;
    }

    public void setReportDesignerJavascript(String reportDesignerJavascript) {
        this.reportDesignerJavascript = reportDesignerJavascript;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public String getReportDisplayJavascript() {
        return reportDisplayJavascript;
    }

    public void setReportDisplayJavascript(String reportDisplayJavascript) {
        this.reportDisplayJavascript = reportDisplayJavascript;
    }

    public Map<String, Integer> getReportsDropdown() {
        return reportsDropdown;
    }

    public void setReportsDropdown(Map<String, Integer> reportsDropdown) {
        this.reportsDropdown = reportsDropdown;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }
}
