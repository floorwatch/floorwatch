package com.floorwatch.web;

import com.floorwatch.entities.Flares;
import com.floorwatch.entities.Stores;
import com.floorwatch.entities.UserCompanies;
import com.floorwatch.entities.UserStores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.FlaresFacade;
import com.floorwatch.entities.manager.StoresFacade;
import com.floorwatch.entities.manager.UsersFacade;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * <H3>StoreDetailsBean</H3>
 *
 * ManagedBean for the FloorWatch store details
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="storeDetailsBean")
@ViewScoped
public class StoreDetailsBean {
    
    private Integer storeId;
    private Stores store;
    private List<Flares> unresolvedFlares;
    private List<Flares> resolvedFlares;
    private String backUrl;
    private static Logger log = Logger.getLogger(StoreDetailsBean.class);

    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create StoreDetailsBean
     *
     * @author  Dale Davis
     * @date    10/13/2015
     */
    public StoreDetailsBean() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            navigationBean.setMainSelectedIndex(1);
        } else {
            navigationBean.setMainSelectedIndex(0);
        }
        backUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                + "/admin";        
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        // Initialize
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        UsersFacade uf = new UsersFacade();
        StoresFacade sf = new StoresFacade();
        FlaresFacade ff = new FlaresFacade();
        setUnresolvedFlares(new ArrayList<Flares>());
        setResolvedFlares(new ArrayList<Flares>());
        
        // Get the store id from CGI
        try {
            Integer cgiStoreId = (Integer) Integer.parseInt((String)FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("storeId"));
            setStoreId(cgiStoreId);
        } catch(NumberFormatException nfe) {}
        
        // Validate access
        Subject currentUser = SecurityUtils.getSubject();
        Users user = null;
        Integer currentUserId = navigationBean.getCurrentUserId();
        try {
            user = uf.findById(currentUserId);
        } catch(NoResultException nre) {
        } catch(Exception e) {}
        if (currentUser.hasRole("Corporate")) {
            boolean storeFound = false;
            for (UserCompanies company :user.getUserCompaniesList()) {
                for (Stores store :company.getCompanyId().getStoresList()) {
                    if (store.getId().intValue() == getStoreId().intValue()) {
                        storeFound = true;
                        break;
                    }
                }
            }
            if (!storeFound) {
                try {
                    context.getExternalContext().responseSendError(403, "Invalid request.");
                    context.responseComplete();
                    return;
                } catch(Exception e) {}                
            }
        }
        if (currentUser.hasRole("Manager")) {
            boolean storeFound = false;
            for (UserStores store : user.getUserStoresList()) {
                if (store.getStoreId().getId().intValue() == getStoreId().intValue()) {
                    storeFound = true;
                    break;
                }
            }
            if (!storeFound) {
                try {
                    context.getExternalContext().responseSendError(403, "Invalid request.");
                    context.responseComplete();
                    return;
                } catch(Exception e) {}                
            }            
        }
        // Get store info
        try {
            setStore(sf.findById(getStoreId()));
            setUnresolvedFlares(ff.findUnresolvedByStoreMidnight(getStoreId()));
            setResolvedFlares(ff.findResolvedByStoreMidnight(getStoreId()));
        } catch(Exception e) {
        }
    } 
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/    
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public Stores getStore() {
        return store;
    }

    public void setStore(Stores store) {
        this.store = store;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public List<Flares> getUnresolvedFlares() {
        return unresolvedFlares;
    }

    public void setUnresolvedFlares(List<Flares> unresolvedFlares) {
        this.unresolvedFlares = unresolvedFlares;
    }

    public List<Flares> getResolvedFlares() {
        return resolvedFlares;
    }

    public void setResolvedFlares(List<Flares> resolvedFlares) {
        this.resolvedFlares = resolvedFlares;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public void setBackUrl(String backUrl) {
        this.backUrl = backUrl;
    }

}
