package com.floorwatch.web;

import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.UsersFacade;
import com.floorwatch.common.utils.UserUtilities;
import java.io.IOException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.util.SavedRequest;
import org.apache.shiro.web.util.WebUtils;

/**
 * <H3>LoginBean</H3>
 *
 * ManagedBean for the FloorWatch user login
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="loginBean")
@ViewScoped
public class LoginBean implements Serializable {
    
    private static final long serialVersionUID = 7863262235394607247L;
    private String username;
    private String password;
    private static Logger log = Logger.getLogger(LoginBean.class);

    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    public LoginBean() {
        data2Form();
    }    
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    private void addErrorMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
    }
    
    private void addInfoMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }     
    
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        NavigationHandler navigator = context.getApplication().getNavigationHandler();
        Subject currentUser = SecurityUtils.getSubject();        
        if (currentUser.isAuthenticated()) {
            
            if (currentUser.hasRole("Administration") || currentUser.hasRole("Corporate")) {
                navigator.handleNavigation(context, null, "pretty:admin");
            } else {
                NavigationBean navigationBean = (NavigationBean) context.getApplication().
                    evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
                UsersFacade uf = new UsersFacade();
                Users user = null;
                try {
                    user = uf.findById(navigationBean.getCurrentUserId());
                } catch (NoResultException nre) {
                } catch (Exception e) {
                } 
                try {
                    ec.redirect(ec.getRequestContextPath() + "/store/" + user.getUserStoresList().get(0).getStoreId().getId());          
                } catch(IOException ioe) {}    
            }            
        }
    }
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/ 
    
    public void login() {
        FacesContext context = FacesContext.getCurrentInstance();
        ExternalContext ec = context.getExternalContext();
        NavigationHandler navigator = context.getApplication().getNavigationHandler();
        UsersFacade uf = new UsersFacade();
        Users user = null;
        try {
            user = uf.findByUsername(username);
        } catch (NoResultException nre) {
            addErrorMessage("Invalid username or password");
            return;
        } catch (Exception e) {
            addErrorMessage("Invalid username or password");
            return;
        }
        String encryptedPassword = UserUtilities.encryptPassword(password, user.getSalt());
        try {
            SecurityUtils.getSubject().login(new UsernamePasswordToken(username, encryptedPassword, false));
        } catch (AuthenticationException ae) {
            addErrorMessage("Invalid username or password");
            return;
        }
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        navigationBean.setCurrentUserId(user.getId());        
        Subject currentUser = SecurityUtils.getSubject();
        SavedRequest savedRequest = WebUtils.getAndClearSavedRequest((HttpServletRequest)ec.getRequest());
        if (savedRequest != null) {
            try {
                ec.redirect(savedRequest.getRequestUrl());
            } catch(IOException ioe) {}    
            return;
        }            
        if (currentUser.hasRole("Administration") || currentUser.hasRole("Corporate")) {
            navigator.handleNavigation(context, null, "pretty:admin");
        } else {
            try {
                ec.redirect(ec.getRequestContextPath() + "/store/" + user.getUserStoresList().get(0).getStoreId().getId());            
            } catch(IOException ioe) {}    
        }    
    } 
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
