package com.floorwatch.web;

import com.floorwatch.entities.Stores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.StoresFacade;
import com.floorwatch.entities.manager.UsersFacade;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * <H3>StoreBean</H3>
 *
 * ManagedBean for the FloorWatch stores
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="storeBean")
@ViewScoped
public class StoreBean {
    
    private List<Stores> stores;
    private String basePath;
    private String newPath;
    private String bulkPath;
    private static Logger log = Logger.getLogger(StoreBean.class);

    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create StoreBean
     *
     * @author  Dale Davis
     * @date    10/5/2015
     */
    public StoreBean() {
        // Determine base path
        basePath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/" + "store";
        newPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/admin/store/new";
        bulkPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/admin/store/bulk";
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        // Initialize
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        setStores(new ArrayList());
        StoresFacade sf = new StoresFacade();
        UsersFacade uf = new UsersFacade();
        // If the user is an admin, show all stores 
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            try {
                setStores(sf.findAll());
            } catch(Exception e) {}
        } 
        // If the user is corporate, show only applicable stores
        if (currentUser.hasRole("Corporate")) {        
            Users user = null;
            Integer currentUserId = navigationBean.getCurrentUserId();
            // Update the user
            try {
                user = uf.findById(currentUserId);
            } catch(NoResultException nre) {
            } catch(Exception e) {}
            setStores(user.getUserCompaniesList().get(0).getCompanyId().getStoresList());
        }
    } 
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/    
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public List<Stores> getStores() {
        return stores;
    }

    public void setStores(List<Stores> stores) {
        this.stores = stores;
    }
    
    public String getEditUrl(Stores store) {
        String editPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                + "/admin/store/" + store.getId();
        return editPath;
    }    

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getNewPath() {
        return newPath;
    }

    public void setNewPath(String newPath) {
        this.newPath = newPath;
    }

    public String getBulkPath() {
        return bulkPath;
    }

    public void setBulkPath(String bulkPath) {
        this.bulkPath = bulkPath;
    }
}
