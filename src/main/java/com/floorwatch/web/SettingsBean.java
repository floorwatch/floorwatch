package com.floorwatch.web;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;

/**
 * <H3>SettingsBean</H3>
 *
 * ManagedBean for Floorwatch Settings tab
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */
@ManagedBean(name = "settings")
@ViewScoped
public class SettingsBean implements Serializable {

    private String settingsDropdown;
    private static Logger log = Logger.getLogger(SettingsBean.class);

    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/
    
    /**
     * Create SettingsBean
     *
     * @author  Dale Davis
     * @date    1/15/2016
     */
    public SettingsBean() {
        // Create the Settings dropdown
        settingsDropdown = "<div><ul>";
        String editPasswordPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + "/edit_password";
        settingsDropdown += "<li><a href=\"" + editPasswordPath + "\">Change Password</a></li>";
        settingsDropdown += "</ul></div>";
    }

    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    /* ===================================================
     * PROTECTED METHODS
     * ===================================================*/

    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/

    /* ===================================================
     * ACTION METHODS
     * ===================================================*/

    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/
    public String getSettingsDropdown() {
        return settingsDropdown;
    }

    public void setSettingsDropdown(String settingsDropdown) {
        this.settingsDropdown = settingsDropdown;
    }
    
}
