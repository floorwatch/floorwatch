package com.floorwatch.web;

import com.floorwatch.entities.Companies;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.CompaniesFacade;
import com.floorwatch.entities.manager.UsersFacade;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 * <H3>ConfigBulkStoreBean</H3>
 *
 * ManagedBean for the FloorWatch bulk store configuration
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="configBulkStoreBean")
@ViewScoped
public class ConfigBulkStoreBean {
    
    private Integer companyId;
    private Map<String, Integer> companiesDropdown; 
    private String companyStyle;
    private String backUrl;
    private UploadedFile file;
    private String fileName;
    private static Logger log = Logger.getLogger(ConfigBulkStoreBean.class);
    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create ConfigStoreBean
     *
     * @author  Dale Davis
     * @date    11/5/2015
     */
    public ConfigBulkStoreBean() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            navigationBean.setMainSelectedIndex(1);
        } else {
            navigationBean.setMainSelectedIndex(0);
        }
        backUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                + "/admin";        
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    private void validateForm() {
        // Reset fields
        companyStyle = "valid-input";

        // Required fields
        boolean requiredFieldsMissing = false;
        
        // Only check the company if the user is an admin
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            if (companyId == null) {
                companyStyle = "invalid-input";
                requiredFieldsMissing = true;
            }
        }
        if (requiredFieldsMissing) {
            throw new RuntimeException("There are required fields missing.");
        }
    }
    
    private void addErrorMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
    }
    
    private void addInfoMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }    
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        CompaniesFacade cf = new CompaniesFacade();
        
        // Create the companies dropdown
        setCompaniesDropdown(new LinkedHashMap<String, Integer>());
        try {
            List<Companies> companies = cf.findAll();
            for (Companies company : companies) {
                getCompaniesDropdown().put(company.getDescription(), company.getId());     
            }
        } catch(Exception e) {}
        companyStyle = "valid-input";
    } 
    
    public void form2Data() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        CompaniesFacade cf = new CompaniesFacade();
        Companies company = null;
        
        // Validate form input
        try {
            validateForm();
        } catch(Exception e) {
            log.info("Config Store Validation Error: " + e.getMessage());
            addErrorMessage(e.getMessage());
            return;
        }
        
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            try {
                company = cf.findById(companyId);
            } catch(NoResultException nre) {
                log.error("Company id not found.");
                addErrorMessage("An error occured while saving the store.");
                return;
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the store.");
                return;                
            }
        } else {     
            // Get the user from the database
            Integer userId = navigationBean.getCurrentUserId();
            UsersFacade uf = new UsersFacade();        
            Users user = null;
            try {
                user = uf.findById(userId);
            } catch(NoResultException nre) {
                log.error("User id not found.");
                addErrorMessage("An error occured while saving the store.");
                return;            
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the store.");
                return;            
            }
            company = user.getUserCompaniesList().get(0).getCompanyId();
        }
        
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }        
        
        // Navigate back to the admin tab list -> Stores
        if (currentUser.hasRole("Administration")) {
            navigationBean.setMainSelectedIndex(1);
        } else {
            navigationBean.setMainSelectedIndex(0);
        }    
        NavigationHandler navigator = context.getApplication().getNavigationHandler();
        navigator.handleNavigation(context, null, "pretty:admin");
    }    
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/
    
    public void handleFileUpload(FileUploadEvent event) {
        file = event.getFile();
        fileName = file.getFileName();
    }    
    
    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/    

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Map<String, Integer> getCompaniesDropdown() {
        return companiesDropdown;
    }

    public void setCompaniesDropdown(Map<String, Integer> companiesDropdown) {
        this.companiesDropdown = companiesDropdown;
    }

    public String getCompanyStyle() {
        return companyStyle;
    }

    public void setCompanyStyle(String companyStyle) {
        this.companyStyle = companyStyle;
    }

    public String getBackUrl() {
        return backUrl;
    }

    public void setBackUrl(String backUrl) {
        this.backUrl = backUrl;
    }

    public UploadedFile getFile() {
        return file;
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

}
