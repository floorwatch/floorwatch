package com.floorwatch.web;

import com.floorwatch.common.utils.UserUtilities;
import com.floorwatch.entities.Companies;
import com.floorwatch.entities.Roles;
import com.floorwatch.entities.Stores;
import com.floorwatch.entities.UserCompanies;
import com.floorwatch.entities.UserRoles;
import com.floorwatch.entities.UserStores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.CompaniesFacade;
import com.floorwatch.entities.manager.RolesFacade;
import com.floorwatch.entities.manager.StoresFacade;
import com.floorwatch.entities.manager.UsersFacade;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

/**
 * <H3>ConfigStoreBean</H3>
 *
 * ManagedBean for the FloorWatch user configuration
 *
 * This file contains proprietary information of FloorWatch, Inc.
 * Copying or reproduction without prior written approval is prohibited.
 */

@ManagedBean(name="configUserBean")
@ViewScoped
public class ConfigUserBean {
    
    private Users loginUser;
    private Integer userId;
    private String username;
    private String usernameStyle;
    private String password;
    private String passwordStyle;
    private String passwordConfirm;
    private String passwordConfirmStyle;
    private String firstName;
    private String firstNameStyle;
    private String lastName;
    private String lastNameStyle;
    private String email;
    private String emailStyle;
    private Integer roleId;
    private Map<String, Integer> rolesDropdown;    
    private String roleStyle;
    private Integer companyId;
    private Map<String, Integer> companiesDropdown;
    private String companyStyle;
    private boolean companiesPanelRendered;
    private Integer storeId;
    private Map<String, Integer> storesDropdown;
    private String storeStyle;
    private boolean storesPanelRendered;
    private String backUrl;
    private static Logger log = Logger.getLogger(ConfigUserBean.class);
    
    /* ===================================================
     * CONSTRUCTOR METHODS
     * ===================================================*/    
    
    /**
     * Create ConfigUserBean
     *
     * @author  Dale Davis
     * @date    12/15/2015
     */
    public ConfigUserBean() {
        FacesContext context = FacesContext.getCurrentInstance();
        // Determine if there is a user id
        if (context.getExternalContext().getRequestParameterMap().get("userId") != null) {
            try {
                userId = (Integer) Integer.parseInt((String)context.getExternalContext().getRequestParameterMap().get("userId"));        
            } catch(NumberFormatException e){
            }    
        }
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        Subject currentUser = SecurityUtils.getSubject();
        if (currentUser.hasRole("Administration")) {
            navigationBean.setMainSelectedIndex(2);
        } else {
            navigationBean.setMainSelectedIndex(1);
        }
        backUrl = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()
                + "/admin";        
        data2Form();
    }
    
    /* ===================================================
     * PRIVATE METHODS
     * ===================================================*/
    
    private void validateForm() {
        UsersFacade uf = new UsersFacade();
        
        // Reset form fields
        usernameStyle = "valid-input";
        passwordStyle = "valid-input";
        passwordConfirmStyle = "valid-input";
        firstNameStyle = "valid-input";
        lastNameStyle = "valid-input";
        emailStyle = "valid-input";
        roleStyle = "valid-input";
        companyStyle = "valid-input";
        storeStyle = "valid-input";

        // Required fields
        boolean requiredFieldsMissing = false;

        if (firstName == null || firstName.length() == 0) {
            firstNameStyle = "invalid-input";
            requiredFieldsMissing = true;
        }

        if (lastName == null || lastName.length() == 0) {
            lastNameStyle = "invalid-input";
            requiredFieldsMissing = true;
        }

        if (email == null || email.length() == 0) {
            emailStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        
        if (roleId  == 0) {
            roleStyle = "invalid-input";
            requiredFieldsMissing = true;
        }
        
        if (companiesPanelRendered) {
            if (companyId == 0) {
                companyStyle = "invalid-input";
                requiredFieldsMissing = true;
            }
        }
        
        if (storesPanelRendered) {
            if (storeId == 0) {
                storeStyle = "invalid-input";
                requiredFieldsMissing = true;
            }
        }
        
        // New user
        if (userId == null) {
            // Required fields
            if (username == null || username.length() == 0) {
                usernameStyle = "invalid-input";
                requiredFieldsMissing = true;
            }
            
            if (password == null || password.length() == 0) {
                passwordStyle = "invalid-input";
                requiredFieldsMissing = true;
            } 
            
            if (passwordConfirm == null || passwordConfirm.length() == 0) {
                passwordConfirmStyle = "invalid-input";
                requiredFieldsMissing = true;
            }             
         
        }

        if (requiredFieldsMissing) {
            throw new RuntimeException("There are required fields missing.");
        }

        // New user
        if (userId == null) {
            
            // Username not available
            Users userCheck = null;
            try {
                userCheck = uf.findByUsername(username);
            } catch(NoResultException nre) {
                // Username is available
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
            }
            if (userCheck != null) {
                throw new RuntimeException("The selected Username is already in use.");
            }
        
            // Password and Password Confirm don't match
            if (!password.equals(passwordConfirm)) {
                throw new RuntimeException("The Password and Password Confirm fields do not match.");
            }
            
        }
    }
    
    private void addErrorMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
    }
    
    private void addInfoMessage(String message) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, message, ""));
    }    
    
    /* ===================================================
     * PUBLIC METHODS
     * ===================================================*/
    
    public void data2Form() {
        companiesPanelRendered = false;
        storesPanelRendered = false;
        UsersFacade uf = new UsersFacade();
        RolesFacade rf = new RolesFacade();
        CompaniesFacade cf = new CompaniesFacade();
        StoresFacade sf = new StoresFacade();
        Users user = null;
        setRolesDropdown(new LinkedHashMap<String, Integer>());
        setCompaniesDropdown(new LinkedHashMap<String, Integer>());
        setStoresDropdown(new LinkedHashMap<String, Integer>());
        Subject credentialUser = SecurityUtils.getSubject();
                FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        
        // Get the current user from the database
        try {
            loginUser = uf.findById(navigationBean.getCurrentUserId());
        } catch (NoResultException nre) {
            log.error("Logged in user id not found.");
            addErrorMessage("An error occured while loading the user information.");
            return;            
        } catch (Exception e) {
            log.error("Database error: " + e.getMessage(), e);
            addErrorMessage("An error occured while loading the user information.");
            return;            
        }

        // If the user exists, get from the database
        if (getUserId() != null) {
            
            try {
                user = uf.findById(getUserId());
            } catch(NoResultException nre) {
                log.error("User id not found.");
                addErrorMessage("An error occured while loading the user information.");
                return;                
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while loading the user information.");
                return;                
            }
            username = user.getUsername();
            firstName = user.getFirstName();
            lastName = user.getLastName();
            email = user.getEmailAddress();
            Roles role = user.getUserRolesList().get(0).getRoleId();
            roleId = user.getUserRolesList().get(0).getRoleId().getId();
            if (role.getDescription().equalsIgnoreCase("corporate")) {
                companyId = user.getUserCompaniesList().get(0).getCompanyId().getId();
            }
            if (role.getDescription().equalsIgnoreCase("manager")) {
                storeId = user.getUserStoresList().get(0).getStoreId().getId();
                companyId = user.getUserStoresList().get(0).getStoreId().getCompanyId().getId();
            }
        }
        
        // Create the roles dropdown for admin user
        if (credentialUser.hasRole("Administration")) {
            // Type
            try {
                List<Roles> roles = rf.findAll();
                for (Roles role : roles) {
                    // Cannot create a basic user from UI
                    if (role.getDescription().equalsIgnoreCase("basic")) {
                        continue;
                    }
                    getRolesDropdown().put(role.getDescription(), role.getId());     
                }    
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while loading the user information.");
                return;                
            }
            // If existing user, determine if company and store panel needs to
            // be rendered
            if (companyId != null) {
                try {
                    List<Companies> companies = cf.findAll();
                    for (Companies company : companies) {
                        getCompaniesDropdown().put(company.getDescription(), company.getId());     
                    }
                } catch(Exception e) {
                    log.error("Database error: " + e.getMessage(), e);
                    addErrorMessage("An error occured while loading the user information.");
                    return;                    
                }    
                companiesPanelRendered = true;
            }
            if (storeId != null) {
                try {
                    List<Stores> stores = sf.findByCompanyId(companyId);
                    for (Stores store : stores) {
                        getStoresDropdown().put(store.getDescription() + " " + store.getAddress1(), store.getId());     
                    }    
                } catch(Exception e) {
                    log.error("Database error: " + e.getMessage(), e);
                    addErrorMessage("An error occured while loading the user information.");
                    return;                    
                }                
                storesPanelRendered = true;
            }
        }
        
        // Create the stores dropdown for corporate user
        if (credentialUser.hasRole("Corporate")) {        
            try {
                List<Stores> stores = sf.findByCompanyId(loginUser.getUserCompaniesList().get(0).getId());
                for (Stores store : stores) {
                    getStoresDropdown().put(store.getDescription() + " " + store.getAddress1(), store.getId());     
                }    
            } catch(Exception e) {}
            storesPanelRendered = true;
        }
        
        usernameStyle = "valid-input";
        passwordStyle = "valid-input";
        passwordConfirmStyle = "valid-input";
        firstNameStyle = "valid-input";
        lastNameStyle = "valid-input";
        emailStyle = "valid-input";
        roleStyle = "valid-input";
        companyStyle = "valid-input";
        storeStyle = "valid-input";
    } 
    
    public void form2Data() {
        FacesContext context = FacesContext.getCurrentInstance();
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);
        UsersFacade uf = new UsersFacade();
        Users user = null;
        RolesFacade rf = new RolesFacade();
        Roles role = null;
        CompaniesFacade cf = new CompaniesFacade();
        Companies company = null;
        StoresFacade sf = new StoresFacade();
        Stores store = null;
        Subject currentUser = SecurityUtils.getSubject();
        
        // Validate form input
        try {
            validateForm();
        } catch(Exception e) {
            log.info("Config User Validation Error: " + e.getMessage());
            addErrorMessage(e.getMessage());
            return;
        }
        
        // Get the user from the database if this is an edit
        if (userId != null) {
            try {
                user = uf.findById(getUserId());
            } catch(NoResultException nre) {
                log.error("User id not found.");
                addErrorMessage("An error occured while saving the user.");
                return;
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the user.");
                return;                
            }
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmailAddress(email);
            UserRoles userRole = user.getUserRolesList().get(0);
            
            // Get the selected role from database
            try {
                role = rf.findById(roleId);
                userRole.setRoleId(role);
                userRole.setUpdatedAt(new Date());
                userRole.setUpdatedBy(loginUser.getId());
            } catch(NoResultException nre) {
                log.error("Role id not found.");
                addErrorMessage("An error occured while saving the user.");
                return;                
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the user.");
                return;                
            }
            // If the user is a corporate user, save the company        
            if (role.getDescription().equalsIgnoreCase("corporate")) {
                try {
                    company = cf.findById(companyId);
                } catch(NoResultException nre) {
                    log.error("Company id not found.");
                    addErrorMessage("An error occured while saving the user.");
                    return;
                } catch(Exception e) {
                    log.error("Database error: " + e.getMessage(), e);
                    addErrorMessage("An error occured while saving the user.");
                    return;                    
                }
                if (user.getUserCompaniesList() != null && !user.getUserCompaniesList().isEmpty()) {
                    UserCompanies userCompany = user.getUserCompaniesList().get(0);
                    userCompany.setCompanyId(company);
                    userCompany.setUpdatedAt(new Date());
                    userCompany.setUpdatedBy(loginUser.getId());
                } else {
                    user.setUserCompaniesList(new ArrayList());
                    UserCompanies userCompany = new UserCompanies();
                    userCompany.setUserId(user);
                    userCompany.setCompanyId(company);
                    userCompany.setCreatedAt(new Date());
                    userCompany.setCreatedBy(loginUser.getId());
                    userCompany.setUpdatedAt(new Date());
                    userCompany.setUpdatedBy(loginUser.getId());
                    user.getUserCompaniesList().add(userCompany);
                }
            }
            // If the user is a manager user, save the store
            if (role.getDescription().equalsIgnoreCase("manager")) {
                try {
                    store = sf.findById(storeId);
                } catch(NoResultException nre) {
                    log.error("Store id not found.");
                    addErrorMessage("An error occured while saving the user.");
                    return;                    
                } catch(Exception e) {
                    log.error("Database error: " + e.getMessage(), e);
                    addErrorMessage("An error occured while saving the user.");
                    return;                    
                }
                if (user.getUserStoresList() != null && !user.getUserStoresList().isEmpty()) {
                    UserStores userStore = user.getUserStoresList().get(0);
                    userStore.setStoreId(store);
                    userStore.setUpdatedAt(new Date());
                    userStore.setUpdatedBy(loginUser.getId());
                } else {
                    user.setUserCompaniesList(new ArrayList());
                    UserStores userStore = new UserStores();
                    userStore.setUserId(user);
                    userStore.setStoreId(store);
                    userStore.setCreatedAt(new Date());
                    userStore.setCreatedBy(loginUser.getId());
                    userStore.setUpdatedAt(new Date());
                    userStore.setUpdatedBy(loginUser.getId());
                    user.getUserStoresList().add(userStore);
                }                
            }
            user.setUpdatedBy(loginUser.getId());
            user.setUpdatedAt(new Date());
        } else {
            user = new Users();
            user.setUsername(username);
            String salt = UserUtilities.generateSalt();
            user.setSalt(salt);
            user.setPassword(UserUtilities.encryptPassword(password, salt));
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmailAddress(email);
            // Get the selected role from database
            user.setUserRolesList(new ArrayList());
            UserRoles userRole = new UserRoles();
            try {
                role = rf.findById(roleId);
            } catch(NoResultException nre) {
                log.error("Role id not found.");
                addErrorMessage("An error occured while saving the user.");
                return;                
            } catch(Exception e) {
                log.error("Database error: " + e.getMessage(), e);
                addErrorMessage("An error occured while saving the user.");
                return;                
            }
            userRole.setUserId(user);
            userRole.setRoleId(role);
            userRole.setCreatedAt(new Date());
            userRole.setCreatedBy(loginUser.getId());
            userRole.setUpdatedAt(new Date());
            userRole.setUpdatedBy(loginUser.getId());            
            user.getUserRolesList().add(userRole);
            
            // If the user is a corporate user, save the company        
            if (role.getDescription().equalsIgnoreCase("corporate")) {
                try {
                    company = cf.findById(companyId);
                } catch (NoResultException nre) {
                    log.error("Company id not found.");
                    addErrorMessage("An error occured while saving the user.");
                    return;                    
                } catch (Exception e) {
                    log.error("Database error: " + e.getMessage(), e);
                    addErrorMessage("An error occured while saving the user.");
                    return;                    
                }
                user.setUserCompaniesList(new ArrayList());
                UserCompanies userCompany = new UserCompanies();
                userCompany.setUserId(user);
                userCompany.setCompanyId(company);
                userCompany.setCreatedAt(new Date());
                userCompany.setCreatedBy(loginUser.getId());
                userCompany.setUpdatedAt(new Date());
                userCompany.setUpdatedBy(loginUser.getId());
                user.getUserCompaniesList().add(userCompany);
            }
            
            // If the user is a manager user, save the store
            if (role.getDescription().equalsIgnoreCase("manager")) {
                try {
                    store = sf.findById(storeId);
                } catch (NoResultException nre) {
                    log.error("Store id not found.");
                    addErrorMessage("An error occured while saving the user.");
                    return;                    
                } catch (Exception e) {
                    log.error("Database error: " + e.getMessage(), e);
                    addErrorMessage("An error occured while saving the user.");
                    return;                    
                }
                user.setUserStoresList(new ArrayList());
                UserStores userStore = new UserStores();
                userStore.setUserId(user);
                userStore.setStoreId(store);
                userStore.setCreatedAt(new Date());
                userStore.setCreatedBy(loginUser.getId());
                userStore.setUpdatedAt(new Date());
                userStore.setUpdatedBy(loginUser.getId());
                user.getUserStoresList().add(userStore);
            }         
            user.setIsActive(new Short("1"));
            user.setCreatedBy(loginUser.getId());
            user.setCreatedAt(new Date());
            user.setUpdatedBy(loginUser.getId());
            user.setUpdatedAt(new Date());
        }
        // Save
        try {
            uf.save(user);
        } catch(Exception e) {
            log.error("Database error: " + e.getMessage(), e);
            addErrorMessage("An error occured while saving the user.");
            return;            
        }
        // Navigate back to the admin tab list -> Stores
        if (currentUser.hasRole("Administration")) {
            navigationBean.setMainSelectedIndex(2);
        } else {
            navigationBean.setMainSelectedIndex(1);
        }    
        NavigationHandler navigator = context.getApplication().getNavigationHandler();
        navigator.handleNavigation(context, null, "pretty:admin");
    }    
    
    /* ===================================================
     * AJAX METHODS
     * ===================================================*/
    
    public void processTypeChoice() {
        FacesContext context = FacesContext.getCurrentInstance();        
        NavigationBean navigationBean = (NavigationBean) context.getApplication().
                evaluateExpressionGet(context, "#{navigationBean}", NavigationBean.class);        
        RolesFacade rf = new RolesFacade();
        CompaniesFacade cf = new CompaniesFacade();
        Roles role = null;
        
        if (roleId == null) {
            companyId = null;
            storeId = null;
            companiesPanelRendered = false;
            storesPanelRendered = false;
        } else {
            try {
                role = rf.findById(roleId);
            } catch(NoResultException nre) {
                log.error("Role id not found");
                return;
            } catch(Exception e) {
                log.error("Database Error: " + e.getMessage(), e);
                return;
            }
            if (role.getDescription().equalsIgnoreCase("administration")) {
                companyId = null;
                storeId = null;
                companiesPanelRendered = false;
                storesPanelRendered = false;
            } else {
                companyId = null;
                storeId = null;
                List<Companies> companies = new ArrayList<Companies>();
                try {
                    companies = cf.findAll();
                } catch(Exception e) {
                    log.error("Database Error: " + e.getMessage(), e);
                    return;                    
                }    
                for (Companies company : companies) {
                    getCompaniesDropdown().put(company.getDescription(), company.getId());     
                }
                companiesPanelRendered = true;
                storesPanelRendered = false;
            } 
        }
        
    }
    
    public void processCompanyChoice() {
        RolesFacade rf = new RolesFacade();
        StoresFacade sf = new StoresFacade();
        Roles role = null;
        try {
            role = rf.findById(roleId);
        } catch (NoResultException nre) {
            log.error("Role id not found");
            return;
        } catch (Exception e) {
            log.error("Database Error: " + e.getMessage(), e);
            return;
        }
        if (role.getDescription().equalsIgnoreCase("corporate")) {
            storeId = null;
            storesPanelRendered = false;
        } else {
            storeId = null;
            try {
                List<Stores> stores = new ArrayList<Stores>();
                try {
                    stores = sf.findByCompanyId(companyId);
                } catch(Exception e) {
                    log.error("Database Error: " + e.getMessage(), e);
                    return;                    
                }    
                for (Stores store : stores) {
                    getStoresDropdown().put(store.getDescription() + " " + store.getAddress1(), store.getId());
                }
            } catch (Exception e) {
            }
            storesPanelRendered = true;
        }
    }

    /* ===================================================
     * ACCESSOR METHODS
     * ===================================================*/
    public String getBackUrl() {
        return backUrl;
    }

    public void setBackUrl(String backUrl) {
        this.backUrl = backUrl;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsernameStyle() {
        return usernameStyle;
    }

    public void setUsernameStyle(String usernameStyle) {
        this.usernameStyle = usernameStyle;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstNameStyle() {
        return firstNameStyle;
    }

    public void setFirstNameStyle(String firstNameStyle) {
        this.firstNameStyle = firstNameStyle;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastNameStyle() {
        return lastNameStyle;
    }

    public void setLastNameStyle(String lastNameStyle) {
        this.lastNameStyle = lastNameStyle;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailStyle() {
        return emailStyle;
    }

    public void setEmailStyle(String emailStyle) {
        this.emailStyle = emailStyle;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordStyle() {
        return passwordStyle;
    }

    public void setPasswordStyle(String passwordStyle) {
        this.passwordStyle = passwordStyle;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getPasswordConfirmStyle() {
        return passwordConfirmStyle;
    }

    public void setPasswordConfirmStyle(String passwordConfirmStyle) {
        this.passwordConfirmStyle = passwordConfirmStyle;
    }

    public Map<String, Integer> getRolesDropdown() {
        return rolesDropdown;
    }

    public void setRolesDropdown(Map<String, Integer> rolesDropdown) {
        this.rolesDropdown = rolesDropdown;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleStyle() {
        return roleStyle;
    }

    public void setRoleStyle(String roleStyle) {
        this.roleStyle = roleStyle;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Integer companyId) {
        this.companyId = companyId;
    }

    public Map<String, Integer> getCompaniesDropdown() {
        return companiesDropdown;
    }

    public void setCompaniesDropdown(Map<String, Integer> companiesDropdown) {
        this.companiesDropdown = companiesDropdown;
    }

    public String getCompanyStyle() {
        return companyStyle;
    }

    public void setCompanyStyle(String companyStyle) {
        this.companyStyle = companyStyle;
    }

    public Integer getStoreId() {
        return storeId;
    }

    public void setStoreId(Integer storeId) {
        this.storeId = storeId;
    }

    public Map<String, Integer> getStoresDropdown() {
        return storesDropdown;
    }

    public void setStoresDropdown(Map<String, Integer> storesDropdown) {
        this.storesDropdown = storesDropdown;
    }

    public String getStoreStyle() {
        return storeStyle;
    }

    public void setStoreStyle(String storeStyle) {
        this.storeStyle = storeStyle;
    }

    public boolean isCompaniesPanelRendered() {
        return companiesPanelRendered;
    }

    public void setCompaniesPanelRendered(boolean companiesPanelRendered) {
        this.companiesPanelRendered = companiesPanelRendered;
    }

    public boolean isStoresPanelRendered() {
        return storesPanelRendered;
    }

    public void setStoresPanelRendered(boolean storesPanelRendered) {
        this.storesPanelRendered = storesPanelRendered;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Users getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(Users loginUser) {
        this.loginUser = loginUser;
    }

}
