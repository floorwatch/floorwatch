package com.floorwatch.web;

import com.floorwatch.entities.UserRoles;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.UsersFacade;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import javax.persistence.NoResultException;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;

public class AuthenticationRealm extends AuthorizingRealm {
    
    private Users user;
    
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
        PrincipalCollection principals;
        UsersFacade uf = new UsersFacade();
        try {
            user = uf.findByUsername(authcToken.getPrincipal().toString());
        } catch(NoResultException nre) {
            throw nre;
        } catch(Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        try {
            principals = createPrincipals(user);
        } catch (Exception e) {
            throw new AuthenticationException("Unable to obtain authenticated account properties.", e);
        }
        return new SimpleAuthenticationInfo(principals, user.getPassword());
    }
    
    protected PrincipalCollection createPrincipals(Users user) {
        LinkedHashMap<String, String> props = new LinkedHashMap<String, String>();
        props.put("username", user.getUsername());
        Collection<Object> principals = new ArrayList<Object>();
        principals.add(props);
        return new SimplePrincipalCollection(principals, getName());
    }    
    
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        for (UserRoles role : user.getUserRolesList()) {
            info.addRole(role.getRoleId().getDescription());
        }    
        return info;
    }
    
    
}
